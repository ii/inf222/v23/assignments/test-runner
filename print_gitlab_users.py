import csv

with open('students.csv', 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        print(f'{row["gituser"]:30s}: {row["name"]} <{row["email"]}>')

