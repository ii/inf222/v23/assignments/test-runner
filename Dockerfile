FROM haskell:9-slim-buster AS haskell-base
ARG GHC

RUN rm -rf /opt/ghc/*/share/doc
FROM debian:buster-slim AS haskell-main
# common haskell + stack dependencies
RUN echo deb http://deb.debian.org/debian bullseye main >> /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y --no-install-recommends tzdata \
        ca-certificates curl dpkg-dev git gcc gnupg g++ libc6-dev \
        libffi-dev libgmp-dev libnuma-dev libtinfo-dev make netbase xz-utils zlib1g-dev && \
    rm -f /etc/localtime && ln -s /usr/share/zoneinfo/Europe/Oslo /etc/localtime && echo Europe/Oslo > /etc/timezone && \
    rm -rf /var/lib/apt/lists/* || true

COPY --from=haskell-base  /opt /opt
COPY --from=haskell-base  /usr/local /usr/local
RUN ln -s /opt/ghc/*/bin/* /usr/local/bin/


RUN addgroup --system curry && adduser --system --ingroup curry --home /home/curry curry
USER curry
WORKDIR /home/curry
#ENV PATH /home/curry/bin:/home/curry/.local/bin:/opt/ghc/${GHC}/bin:$PATH

#Haskell dependencies
RUN cabal update \
    && cabal install --lib QuickCheck \
    && cabal install --lib tasty \
    && cabal install --lib tasty-quickcheck \
    && cabal install --lib tasty-hunit \
    && cabal install --lib tasty-ant-xml \
    && cabal install --lib bytestring \
    && cabal install --lib tasty-html \
    && cabal install --lib silently \
    && cabal install --lib haskeline \
    ; rm -f .cabal/packages/hackage.haskell.org/01-*
COPY --chown=curry:curry . /home/curry/
CMD ["/home/curry/run-tests", "setup", "compile", "test"]
