#! /usr/bin/python

import re
import gitlab
import sys
from gitlab import Gitlab, GitlabError
from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects import User, CurrentUser, Project, Group
import csv
from shutil import copyfile

import logging
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

logger.info('hello!')

TOKEN = open('../uib-git.api_token').read().strip()

with open('students.csv', 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    rows = [row for row in reader]

gl = Gitlab(url='https://git.app.uib.no/', private_token=TOKEN)
gl.auth()
missing=[]
unknown=[]

def format_user(row):
    return f"{row['sortable_name']} <{row['email']}> ({row['kind']})"
for row in rows:
    name = row['sortable_name']
    (lastname, firstname) = [s.strip() for s in name.split(',', 1)]
    (first_firstname,*more_firstnames) = firstname.split()
    if row.get('gitid','') == '':
        row['gituser'] = ''

        users = gl.users.list(username=row['email'].replace('@uib.no','').replace('@student.uib.no',''))
        user = None
        if len(users) == 1:
            user = users[0]
        else:
            more_users = gl.users.list(username=row['login_id'])
            if len(more_users) == 1:
                user = more_users[0]
            else:
                more_users = gl.users.list(username=f'{first_firstname}.{lastname}')
                if len(more_users) == 1:
                    user = more_users[0]

        if user:
            row['gituser'] = user.username
            row['gitid'] = user.id
            print("Found", format_user(row))
        elif len(users) == 0:
            if 'uib.no' in row['email']:
                missing.append(row)            
                print("Maybe not registered?", format_user(row))
            else:
                unknown.append(row)
                print("Not found", format_user(row))
        else:
            unknown.append(row)
            print('Ambiguous:', format_user(row), users)

if len(missing) > 0:
    with open('missing.txt', 'w') as f:
        f.writelines([format_user(row)+'\n' for row in missing])
if len(unknown) > 0:
    with open('unknown.txt', 'w') as f:
        f.writelines([format_user(row)+'\n' for row in unknown])

try:
    copyfile('students.csv', 'students.csv.old')
except:
    pass

fieldnames = [*reader.fieldnames]
if 'gituser' not in fieldnames:
    fieldnames.append('gituser')
if 'gitid' not in fieldnames:
    fieldnames.append('gitid')
if 'gitoverride' not in fieldnames:
    fieldnames.append('gitoverride')

with open('students.csv','w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames)
    writer.writeheader()
    writer.writerows(rows)
